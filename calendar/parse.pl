#!/usr/bin/perl

use strict;
use warnings;

# GESTION DES PATH
my $home = $ENV{HOME};
my $dir = $home . "/fichiers_configuration/calendar/";
my $calendar = $dir . "republican_calendar";
my $cal; # descripteur du fichier $calendar

# RÉCUPÉRATION DE LA DATE ACTUELLE ET FORMATTAGE
my $date = `date`;
my @trunc_date = split(/ /, $date);
my $day = $trunc_date[1];
my $month = $trunc_date[2];
# ligne suivante à changer le jour du passage à l'an 10.000
# en profiter pour faire une giga teuf !
my $year = substr $trunc_date[3], 0, 4;
my $bis = $year % 4;

# VARIABLES GLOBALES QUI SERONT UTILES
my $republican_month;
my $republican_day;
my $republican_dayname;

if ( ! open($cal,'<', "$calendar") ) {
  exit(1);
}

while ( <$cal> ) {
  chomp ;
  if ( ! ("$_" =~ m/[0-9].*/) ) {
    $republican_month = $_;
  }
  else {
    if ("$_" =~ m/([0-9]+)\s$day\s$month\s(.*)/ ) {
      print "$1 du mois de $republican_month, jour $2\n";
    }
  }
}
